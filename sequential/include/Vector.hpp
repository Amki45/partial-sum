#pragma once
#include <ostream>

template<class T, size_t Size>
class Vector{
    private:
        T m_data[Size];
    public:
        // Constructors
        Vector(){}
        // Destructor
        // ~Vector(){delete[] m_data;}

        // Getters
        size_t size() const {return Size;}
        T get(size_t t_size) const {return m_data[t_size];}
        T & get(size_t t_size){return m_data[t_size];}
        T * getData(){return m_data;}
        // Setters
        void set(size_t & t_pos, T & t_value){m_data[t_pos] = t_value;}
};
// Other operators
template<class T, size_t Size>
std::ostream& operator<<(std::ostream& out, const Vector<T,Size>& vector) {
    out << "(" << vector.get(0);
    for(size_t i = 1; i<Size; ++i)
        out << ", " << vector.get(i);
    return out << ")\n";
}