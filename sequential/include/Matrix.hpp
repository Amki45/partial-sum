#pragma once
#include "Vector.hpp"
#include <ostream>

template<class T, size_t NbRow, size_t NbCol>
class Matrix{
    private:
        T m_data[NbRow*NbCol];
    public:
        // Constructor
        Matrix(){}
        // Destructor
        // ~Matrix(){delete[] m_data;}

        // Initialization methods
        void init(T t_default_value){
            for(T &elem : m_data)
                elem = t_default_value;
        }
        void init(T (&t_default_data)[NbRow*NbCol]){
            for(size_t i; i<sizeof(t_default_data)+1; ++i)
                m_data[i] = t_default_data[i];
        }

        // Getters
        size_t getNbRow() {return NbRow;}
        size_t getNbCol() {return NbCol;}

        T & get(size_t t_row, size_t t_col){return m_data[t_row*NbCol + t_col];}
        T get(size_t t_row, size_t t_col) const {return m_data[t_row*NbCol + t_col];}

        // Setters
        void set(size_t t_row, size_t t_col, T t_value){m_data[t_row*NbCol + t_col] = t_value;}

        // Other methods
        Vector<T,NbCol> getSum() const {
            Vector<T,NbCol> vector;
            for(size_t col = 0; col<NbCol; ++col){
                T val = get(0, col);
                for(size_t row = 1; row<NbRow; ++row)
                    val += get(row,col);
                vector.set(col, val);
            }
            return vector;
        }
};
// Other operators
template<class T, size_t NbRow, size_t NbCol>
std::ostream& operator<<(std::ostream& out, const Matrix<T,NbRow,NbCol>& matrix){
    for(size_t row = 0; row<NbRow; ++row){
        out << "(" << matrix.get(row, 0);
        for(size_t col = 1; col<NbCol; ++col){
            out << ", " << matrix.get(row, col);
        }
        out << ")\n"; 
    }
    return out;
}