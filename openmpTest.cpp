// OpenMP header 
#include <omp.h>
#include <iostream>
  
int main() 
{ 
    int nthreads, tid;

    int i = 0;
    #pragma omp parallel num_threads(4) private(tid)
    {
        ++i;
        tid = omp_get_thread_num()+1;
        printf("Yo %d de numero %d\n", i, tid);
    }
}