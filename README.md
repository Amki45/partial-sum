# Partial Sum

A C++ program that allows to each columns of a Matrix and outputs a Vector.
This subject concerns my internship, and it is included in the project files.

Implementations:
- Sequential and Scalar
- Parallel
- Generalization (templates)