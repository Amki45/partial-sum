#include "../include/Matrix.hpp"
#include "../include/Vector.hpp"
#include <iostream>
#include <chrono>

#define ROW 10
#define COL 10

int main(){
    time_t start, end;
    time(&start);

    Matrix<unsigned int,5,5> a;
    a.init(1.0f);
    Vector<unsigned int,5> v = a.getSum();
    std::cout << "First Matrix\n" << a << std::endl;
    std::cout << "Corresponding Vector\n" << v << std::endl;

    float tab[9] = {1.0f,2.0f,3.0f,4.0f,5.0f,6.0f,7.0f,8.0f,9.0f};
    Matrix<float,3,3> b;
    b.init(tab);

    std::cout << "Second Matrix\n" << b << std::endl;
    std::cout << "Corresponding Vector\n" << b.getSum() << std::endl;

    // Destructor passing here and stopping the program... Why ?

    int tab2[18] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};
    Matrix<int,3,6> c;
    c.init(tab2);

    std::cout << "Third Matrix\n" <<c << std::endl;
    std::cout << "Corresponding Vector\n" << c.getSum() << std::endl;

    int tab3[ROW*COL];
    for(int i = 0; i<ROW*COL; ++i)
        tab3[i]=i;
    Matrix<int,ROW,COL> d;
    d.init(tab3);

    time(&end);
    double duration = end - start;
    std::cout << "Main execution time in seconds : " << duration << std::endl;

    return 0;
}